# pywol

#### 介绍
基于 https://github.com/remcohaszing/pywakeonlan 修改的支持多网卡的网络唤醒脚本。

#### 使用说明
`wol.py <MAC>`

示例：

`wol.py AA:BB:CC:DD:EE:FF`

